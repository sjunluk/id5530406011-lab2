package junluk.supachai.lab2;

public class AreaVolumeConeSolver {

	public static void main(String[] args) {
		if (args.length==3) {
			double r,s,h;
			r = Double.parseDouble(args[0]);
			s = Double.parseDouble(args[1]);
			h = Double.parseDouble(args[2]);
			double Area = (Math.PI * r * s) + (Math.PI * r * r);
			double Volume = (1.0/3.0) * Math.PI * r * r * h;
			System.out.println("For cone with " + " r " + args[0] + " s " + args[1] + " h " + args[2]);
			System.out.println("Surface area is " + Area + " Volume is " + Volume);
			
		}else
			System.err.println("AreaVolumeConeSolver <r> <s> <h> ");

	}

}
