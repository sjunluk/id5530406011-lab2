package junluk.supachai.lab2;

public class UsingStringAPI {
	public static void main(String[] args) {
		String name = args[0];
		String namename = name.toLowerCase();
		int college = namename.indexOf("college");
		int school = namename.indexOf("school");
		if(namename.indexOf("college") > -1){
			System.out.println(name + " has college at " + college);
		}else if(namename.indexOf("school") > -1){
			System.out.println(name + " has school at " + school);
		}else
			System.out.println(name + " does not contain school or college.");
		
	}

}
