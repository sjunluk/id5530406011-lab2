package junluk.supachai.lab2;

import java.util.Arrays;


public class SortNumbers {
	public static void main(String[] args) {
		double[] number = { Double.parseDouble(args[0]),
				Double.parseDouble(args[1]),
				Double.parseDouble(args[2]),
				Double.parseDouble(args[3]) };
		System.out.println("For input numbers: ");
		for (int i = 0; i < number.length; i++)
			System.out.print(number[i] + " ");
		Arrays.sort(number);
		System.out.println();
		System.out.println("Scorted numbers:");
		for (int i = 0; i < number.length; i++)
			System.out.print(number[i] + " ");
		}
		

	}



